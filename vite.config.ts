import { defineConfig } from 'vitest/config'
import { splitVendorChunkPlugin } from 'vite'
import react from '@vitejs/plugin-react'

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [react(), splitVendorChunkPlugin()],
  test: {
    coverage: {
      provider: 'c8',
      reporter: ['html']
    }
  }
})
