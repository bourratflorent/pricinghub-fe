import {useEffect, useState} from 'react'
import './App.css'
import {Chart} from 'react-chartjs-2';
import {Chart as ChartJS, ChartData, registerables} from 'chart.js';
import DataService from "./service/DataService";
import FilterTree from "./type/FilterTree";
import CsvDataContent from "./type/CsvDataContent";
import TreeFilters from "./ui/TreeFilters";

const dataService = new DataService();

ChartJS.register(...registerables);

const chartOptions = {
    scales : {
        y : {
            min: 0,
            max: 5000
        }
    },
    responsive: true,
    preserveAspectRatio: false
}

function App() {
    const [data, setData] = useState<CsvDataContent[]>([]);
    const [treeCategories, setTreeCategories] = useState<FilterTree[]>([]);
    const [dateList, setDateList] = useState<string[]>([]);
    const [graphData, setGraphData] = useState<ChartData<'line'>>({
        labels: [],
        datasets: [
            {
                label: "Ventes",
                backgroundColor: "rgb(255, 99, 132)",
                borderColor: "rgb(255, 99, 132)",
                data: [],
            },
        ],
    });

    useEffect(() => {
        dataService.getData().then((e) => {
            if (!e || !e.data) {
                return;
            }

            setTreeCategories(e.filters);
            setData(e.data);
            setDateList(e.dates);
        })
    }, []);

    const handleCategoryClick = (c: string[]) => {
        updateData(c);
    }

    const updateData = (checkedFilters: string[]) => {

        const filtered = data.filter((e) => {
            return checkedFilters.includes(e.niveau_1) || checkedFilters.includes(e.niveau_2) || checkedFilters.includes(e.niveau_3)
        });

        let newGraphData: number[] = []
        dateList.forEach((d) => {
            const total = 0;
            newGraphData.push(filtered.filter((e) => e.date === d).reduce((total, sum) => total + Number(sum.ventes), total));
        })


        setGraphData({
            labels: dateList,
            datasets: [
                {
                    label: "Ventes",
                    backgroundColor: "rgb(255, 99, 132)",
                    borderColor: "rgb(255, 99, 132)",
                    data: newGraphData,
                },
            ],
        });

    }

    return (
        <div className='m-10'>
            <div className="w-full flex justify-around flex-wrap">
                <div className='w-full sm:w-2/5 md:w-1/5'>
                    <TreeFilters categories={treeCategories} onCategorySelect={(c) => handleCategoryClick(c)}/>
                </div>

                <div className='w-full sm:w-3/5 md:w-4/5 h-96'>
                    {graphData &&
                        <Chart type={"line"} data={graphData} options={chartOptions}/>
                    }
                </div>
            </div>
        </div>
    )
}

export default App;

