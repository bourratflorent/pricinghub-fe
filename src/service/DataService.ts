import axios from "axios";
import {extractDates, extractFilters, parseCSVtoJSON} from "../helper/CsvHelper";
import FileContent from "../type/FileContent";

class DataService {
    private PATH = "/data.csv"

    async getData(): Promise<FileContent | void> {
        try {
            const response = await axios.get(this.PATH, {responseType: "blob"});

            if (response && response?.data) {
                const file = response.data
                const csvString = await file.text();

                const data = await parseCSVtoJSON(csvString);

                let dateList: string[] = [];
                let filterList: any[] = [];
                if (data) {
                    dateList = extractDates(data);
                    filterList = extractFilters(data);

                }

                return {dates: dateList, data: data, filters: filterList};
            }

            return Promise.resolve();
        } catch (e) {
            console.error(e);
            return Promise.resolve();
        }
    }
}

export default DataService;