import React, {useState} from "react";
import FilterTree from "../type/FilterTree";
import CheckboxTree from "react-checkbox-tree";
import 'react-checkbox-tree/lib/react-checkbox-tree.css';

interface Props {
    categories: FilterTree[];
    onCategorySelect: (category: string[]) => void;
}

export default function TreeFilters({categories, onCategorySelect}: Props) {
    const [checked, setChecked] = useState<string[]>([]);
    const [expanded, setExpanded] = useState<string[]>([]);

    const handleSelect = (c: string[]) => {
        setChecked(c);

        onCategorySelect(c);
    }

    return (
        <>
            {categories &&
                <CheckboxTree
                    nodes={categories}
                    checked={checked}
                    expanded={expanded}
                    onCheck={checked => handleSelect(checked)}
                    onExpand={expanded => setExpanded(expanded)}
                    icons={{
                        check: <span className="rct-icon rct-icon-check"/>,
                        uncheck: <span className="rct-icon rct-icon-uncheck"/>,
                        halfCheck: <span className="rct-icon rct-icon-half-check"/>,
                        expandClose: <span className="rct-icon rct-icon-expand-close"/>,
                        expandOpen: <span className="rct-icon rct-icon-expand-open"/>,
                        expandAll: <span className="rct-icon rct-icon-expand-all"/>,
                        collapseAll: <span className="rct-icon rct-icon-collapse-all"/>,
                        parentClose: '',
                        parentOpen: '',
                        leaf: '',
                    }}
                />
            }
        </>
    );
}
