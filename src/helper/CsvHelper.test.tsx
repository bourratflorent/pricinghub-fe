import { beforeAll, describe, expect, it } from "vitest"
import { extractDates, extractFilters, parseCSVtoJSON } from './CsvHelper';
import CsvDataContent from '../type/CsvDataContent';
import FilterTree from '../type/FilterTree';

describe("parseCSVtoJSON", () => {
    it('should return an empty array when file is not csv', async () => {
        const fileContent = 'aazazaza'

        const actual = await parseCSVtoJSON(fileContent);

        expect(actual).toStrictEqual([])
    })

    it('should return json from CSV', async () => {
        const fileContent =
            `var1;var2
            toto;titi`

        const actual = await parseCSVtoJSON(fileContent);

        expect(actual).toStrictEqual([{ var1: 'toto', var2: 'titi'}])
    })
})

describe("extractDates", () => {
    let dataContent: CsvDataContent;

    beforeAll(() => {
        dataContent = {date: "10/12/2021", niveau_1: 'PC', niveau_2: '13 pouces', niveau_3: 'A', ventes:512}
    })

    it('should return empty array when CsvDataContent is empty', () => {
        const content: CsvDataContent[] = [];

        const actual = extractDates(content);

        expect(actual).toStrictEqual([]);
    })

    it('should return array with one element when all dates are identical', () => {
        const content: CsvDataContent[] = [
            {...dataContent},
            {...dataContent}
        ];

        const actual = extractDates(content);

        expect(actual).toStrictEqual(['10/12/2021']);
    })

    it('should return array with some elements when all dates mismatches', () => {
        const content: CsvDataContent[] = [
            {...dataContent, date:'10/09/2021'},
            {...dataContent, date:'10/10/2021'},
            {...dataContent, date:'10/11/2021'},
            {...dataContent, date:'10/12/2021'},
        ];

        const actual = extractDates(content);

        expect(actual).toStrictEqual(['10/09/2021','10/10/2021','10/11/2021','10/12/2021']);
    })
})

describe('extractFilters', () => {
    let dataContent: CsvDataContent;

    beforeAll(() => {
        dataContent = {date: "10/12/2021", niveau_1: 'PC', niveau_2: '13 pouces', niveau_3: 'A', ventes:512}
    })

    it('should return empty array when content is empty', () => {
        const content: CsvDataContent[] = []

        const actual = extractFilters(content);

        expect(actual).toStrictEqual([])
    })

    it('should return tree array when content is contains children', () => {
        const content: CsvDataContent[] = [
            {...dataContent}
        ]
        const expected: FilterTree[] = [
            {
                label: 'PC',
                value: 'PC',
                level: 1,
                children: [
                    {
                        label: '13 pouces',
                        value: '13 pouces',
                        level: 2,
                        children: [
                            {
                                label: 'A',
                                value: 'A',
                                level: 3
                            }
                        ]}
                ]
            }
        ]

        const actual = extractFilters(content);

        expect(actual).toStrictEqual(expected)
    })

})