import csv from "csvtojson";
import CsvDataContent from "../type/CsvDataContent";
import FilterTree from "../type/FilterTree";

const parseCSVtoJSON = async (fileContent: string, delimiter?: string): Promise<CsvDataContent[]> => {
    return csv({delimiter: delimiter || ';'}).fromString(fileContent);
}

const extractDates = (content: CsvDataContent[]) => {
    let dateList: string[] = [];

    content.forEach((e) => {
        if (dateList.includes(e.date)) {
            return;
        }
        dateList.push(e.date);
    })
    return dateList;
}

const extractFilters = (content: CsvDataContent[]): FilterTree[] => {
    let parentObject: FilterTree[] = [];
    let parentNames: string[] = [];

    content.forEach((e) => {
        if (parentNames && parentNames.includes(e.niveau_1)) {
            return;
        }

        parentNames.push(e.niveau_1);

        parentObject.push({
            label: e.niveau_1,
            value: e.niveau_1,
            level: 1
        })
    })

    parentObject.forEach((e) => {
        const childList = extractChildren(content, e.label, 'niveau_1', 'niveau_2', 2);

        childList.forEach((c) => {
            c.children = extractChildren(content, c.label, 'niveau_2', 'niveau_3', 3);
        })

        e.children = childList;
    })

    return parentObject;
}

const extractChildren = (content: CsvDataContent[], parentName: string, parentFieldName: 'niveau_1' | 'niveau_2', childrenFieldName: 'niveau_2' | 'niveau_3', level: number): FilterTree[] => {
    const filteredContent = content.filter((e) =>  e[parentFieldName] == parentName);
    const childrenList = [...new Set(filteredContent.map(e => { return e[childrenFieldName]}))];
    return childrenList.map(e => { return {label: e, value: e, level: level}})
}

export {parseCSVtoJSON, extractDates, extractFilters};