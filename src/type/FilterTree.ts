type FilterTree = {
    label: string;
    value: string;
    level: number;
    selected?: boolean;
    children?: FilterTree[]
}

export default FilterTree;