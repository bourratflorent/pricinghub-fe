type CsvDataContent = {
    niveau_1: string;
    niveau_2: string;
    niveau_3: string;
    date: string;
    ventes: number;
}

export default CsvDataContent;
