import CsvDataContent from "./CsvDataContent";
import FilterTree from "./FilterTree";

type FileContent = {
    dates: string[];
    data: CsvDataContent[] | null;
    filters: FilterTree[];
}

export default FileContent;